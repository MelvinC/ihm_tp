﻿namespace TP1_IHM
{
    partial class PartitionView
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.SuspendLayout();
            //              
            // LinesView             
            //              
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);             
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;             
            this.BackColor = System.Drawing.Color.White;             
            this.Name = "PartitionView";             
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.PartitionView_MouseDoubleClick);             
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PartitionView_MouseDown);             
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PartitionView_MouseMove);             
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PartitionView_MouseUp);
            this.ResumeLayout(false);
        }

        #endregion
    }
}
