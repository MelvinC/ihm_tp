﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP1_IHM.Componants;

namespace TP1_IHM.Tool
{
    public class SelectTool
    {
        enum State { Initial, Selection }
        private State state;
        private Rectangle lastRectangle;

        public SelectTool()
        {
            state = State.Initial;
        }

        public void MouseDown(PartitionController controller, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                switch (state)
                {
                    case State.Initial:
                        MouseDownInitially(controller, e);
                        break;
                }
            }
        }

        public void MouseUp(PartitionController controller, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                switch (state)
                {
                    case State.Selection:
                        MouseUpAfterSelection(controller, e);
                        break;
                }
            }
        }

        private void MouseUpAfterSelection(PartitionController controller, MouseEventArgs e)
        {
            state = State.Initial;
        }

        public void MouseMove(PartitionController controller, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                switch (state)
                {
                    case State.Selection:
                        MouseMoveAfterSelection(controller, e);
                        break;
                }
            }
        }

        private void MouseDownInitially(PartitionController controller, MouseEventArgs e)
        {
            lastRectangle.Location = e.Location;
            var rectangle = FindRectAtPoint(controller, e.Location);
            bool shift = Control.ModifierKeys == Keys.Shift;
            if (rectangle != null)
            {
                
                if (controller.SelectedPartition.Contains(rectangle))
                {
                    controller.Deselect(rectangle);
                }
                else
                {
                    controller.Select(rectangle, true);
                }
            }
            else
            {
                controller.DeselectAll();
            }
            state = State.Selection;
        }

        private void MouseMoveAfterSelection(PartitionController controller, MouseEventArgs e)
        {
            int deltaX = e.Location.X - lastRectangle.Location.X;
            int deltaY = e.Location.Y - lastRectangle.Location.Y;
            foreach (var part in controller.SelectedPartition)
            {
                part.Offset(deltaX, deltaY);
            }
            lastRectangle.Location = e.Location;
        }
        
        private Partition FindRectAtPoint(PartitionController controller, Point loc)
        {
            var currentSelection = controller.SelectedPartition;
           // var partitions = controller.Disc.Partition.Reverse();
            foreach (var part in controller.Disc.Partition)
            {
                if (part.Rect.Contains(loc))
                {
                    return part;
                }
            }
            return null;
        }
        
        private bool IsRectangleInSomePartition(Partition partition, Rectangle Rectangle)
        {
            double distanceMax = 50/2+2;
            double squaredDistanceMax = distanceMax * distanceMax;
            var Rect = partition.Rect;
            if (Rect == Rectangle)
            {
                return true;
            }
            return false;
        }
        /*
        public static double SquaredDistanceToSegment(Partition.Rectangle p, Partition.Rectangle ps, Partition.Rectangle pe)
        {
            if (ps.X == pe.X && ps.Y == pe.Y) return SquaredDistance(ps, p);
            int sx = pe.X - ps.X;
            int sy = pe.Y - ps.Y;
            int ux = p.X - ps.X;
            int uy = p.Y - ps.Y;
            int dp = sx * ux + sy * uy;
            if (dp < 0) return SquaredDistance(ps, p);
            int sn2 = sx * sx + sy * sy;
            if (sn2 <= dp) return SquaredDistance(pe, p);
            double b = (double)dp / (double)sn2;
            return SquaredDistance(new Partition.Rectangle((int)(ps.X + b * sx), (int)(ps.Y + b * sy)), p);
        }

        public static double SquaredDistance(Rectangle p1, Rectangle p2)
        {
            return (p2.X - p1.X) * (p2.X - p1.X) + (p2.Y - p1.Y) * (p2.Y - p1.Y);
        }
        */
    }
}
