﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP1_IHM.Componants;
using TP1_IHM.Tool;

namespace TP1_IHM
{
    public class SelectionChangedEventArgs : EventArgs
    {
        public SelectionChangedEventArgs(HashSet<Partition> oldSelection)
        {
            OldSelection = oldSelection;
        }

        public virtual HashSet<Partition> OldSelection
        {
            get; set;
        }
    }

    public delegate void SelectionChangedEventHandler(object sender, SelectionChangedEventArgs e);

    public class PartitionController
    {
        private PartitionView view;
        private HashSet<Partition> currentSelection;

        public event SelectionChangedEventHandler SelectionChanged;

        public SelectTool tool { get; set; }
        
        public PartitionController(Disc Disc, PartitionView view)
        {
            this.Disc = Disc;
            this.view = view;
            view.Controller = this;
            currentSelection = new HashSet<Partition>();
        }

        public PartitionView View => view;

        public HashSet<Partition> SelectedPartition
        {
            get
            {
                return currentSelection;
            }
            set
            {
                this.currentSelection = value;
            }
        }

        public Disc Disc { get; set; }

        public void Select(Partition Partition, bool withExtension = false)
        {
            if (withExtension)
            {
                if (currentSelection.Contains(Partition)) return;
                var oldSelection = new HashSet<Partition>(currentSelection);
                currentSelection.Add(Partition);
                OnSelectionChanged(oldSelection);
            }
            else
            {
                var oldSelection = new HashSet<Partition>(currentSelection);
                currentSelection.Clear();
                currentSelection.Add(Partition);
                OnSelectionChanged(oldSelection);
            }
        }

        public void Deselect(Partition Partition)
        {
            if (currentSelection.Contains(Partition))
            {
                var oldSelection = new HashSet<Partition>(currentSelection);
                currentSelection.Remove(Partition);
                OnSelectionChanged(oldSelection);
            }
        }

        public void DeselectAll()
        {
            if (currentSelection.Count > 0)
            {
                var oldSelection = new HashSet<Partition>(currentSelection);
                currentSelection.Clear();
                OnSelectionChanged(oldSelection);
            }
        }

        private void OnSelectionChanged(HashSet<Partition> oldSelection)
        {
            SelectionChangedEventHandler handler = SelectionChanged;
            if (handler != null)
            {
                handler(this, new SelectionChangedEventArgs(oldSelection));
            }
        }
    }
}
