﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TP1_IHM.Componants;

namespace TP1_IHM
{
    public partial class PartitionView : UserControl
    {
        public const int SelectionSquareWidth = 4;
        public const int SelectionSquareHalfWidth = SelectionSquareWidth / 2;

        public PartitionView()
        {
            Controller = new PartitionController(new Disc(), this);
            InitializeComponent();
            Paint += PartitionView_Paint;
            DoubleBuffered = true;
        }

        private void PartitionView_MouseDown(object sender, MouseEventArgs e)
        {
            Controller.tool.MouseDown(Controller, e);
        }

        private void PartitionView_MouseMove(object sender, MouseEventArgs e)
        {
            Controller.tool.MouseMove(Controller, e);
        }

        private void PartitionView_MouseUp(object sender, MouseEventArgs e)
        {
            Controller.tool.MouseUp(Controller, e);
        }
      
        private void PartitionView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //Controller.tool.MouseDoubleClick(Controller, e);
        }

        private void PartitionView_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            var currentSelection = Controller.SelectedPartition;
            foreach (var Partition in Controller.Disc.Partition)
            {
                bool selected = currentSelection.Contains(Partition);
                Draw(Partition, graphics);
                if (selected)
                {
                    Draw(Partition, graphics);
                }
            }
        }

        private void Draw(Partition partition, Graphics graphics)
        {
            Pen pen = new Pen(partition.Color);
            int top = 0;
            foreach(Partition p in this.Controller.Disc.Partition)
            {
                if (p == partition)
                {
                    break;
                }
                top += Disc.SIZE_DISC / p.Size;
            }
            graphics.DrawRectangle(pen, new Rectangle(new Point(0, top), new Size(50,(Disc.SIZE_DISC / partition.Size))));
        }

        private void DrawSelectionSquare(Point point, Graphics graphics)
        {
            Rectangle area = new Rectangle(point, new Size(SelectionSquareWidth, SelectionSquareWidth));
            area.Offset(-SelectionSquareHalfWidth, -SelectionSquareHalfWidth);
            graphics.FillRectangle(Brushes.White, area);
            graphics.DrawRectangle(Pens.Black, area);
        }

        public PartitionController Controller
        {
            get { return Controller; }
            set
            {
                if (Controller != null)
                {
                    Controller.SelectionChanged -= Controller_SelectionChanged;
                    UnobserveDisc(Controller.Disc);
                }
                Controller = value;
                if (Controller != null)
                {
                    Controller.SelectionChanged += Controller_SelectionChanged;
                    ObserveDisc(Controller.Disc);
                }
            }
        }

        private void Controller_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Invalidate();
        }

        private void ObserveDisc(Disc Disc)
        {
            if (Disc == null) return;
            Disc.PartitionAdded += Disc_PartitionAdded;
            Disc.PartitionRemoved += Disc_PartitionRemoved;
            foreach (var Partition in Disc.Partition)
            {
                ObservePartition(Partition);
            }
        }

        private void ObservePartition(Partition Partition)
        {
            if (Partition == null) return;
            Partition.PropertyChanged += Partition_PropertyChanged;
        }

        private void Partition_PropertyChanged(object sender, Componants.PropertyChangedEventArgs e)
        {
            Invalidate();
        }

        private void Disc_PartitionAdded(object sender, PartitionCollectionEventArgs e)
        {
            ObservePartition(e.Partition);
            Invalidate();
        }

        private void Disc_PartitionRemoved(object sender, PartitionCollectionEventArgs e)
        {
            UnobservePartition(e.Partition);
            Invalidate();
        }

        private void UnobserveDisc(Disc Disc)
        {
            if (Disc == null) return;
            foreach (var Partition in Disc.Partition)
            {
                UnobservePartition(Partition);
            }
            Disc.PartitionAdded -= Disc_PartitionAdded;
            Disc.PartitionRemoved -= Disc_PartitionRemoved;
        }

        private void UnobservePartition(Partition Partition)
        {
            Partition.PropertyChanged -= Partition_PropertyChanged;
        }
    }
}
