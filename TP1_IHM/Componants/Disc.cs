﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Drawing;

namespace TP1_IHM.Componants
{
    public class PartitionCollectionEventArgs : EventArgs
    {
        public PartitionCollectionEventArgs(Partition partition) { Partition = partition; }

        public virtual Partition Partition { get; private set; }
    }

    public delegate void PartitionAddedEventHandler(object sender, PartitionCollectionEventArgs e); 
    public delegate void PartitionRemovedEventHandler(object sender, PartitionCollectionEventArgs e);

    public class Disc
    {
        public const int SIZE_DISC = 1000;

        public List<Partition> Partition { get; set; }
        public event PartitionAddedEventHandler PartitionAdded; 
        public event PartitionRemovedEventHandler PartitionRemoved;

        public Disc()
        {
            this.Partition = new List<Partition>();
            this.Partition.Add(new Partition(Disc.SIZE_DISC));
        }

        public Partition[] Partitions => Partitions.ToArray();

        public int size()
        {
            return this.Partition.Count;
        }

        public void AddPartition(int part1, int size2)
        {
            if (this.Partition[part1].Size > size2)
            {
                this.Partition[part1].Size = this.Partition[part1].Size - size2;
                for (int i = this.size(); i < part1 + 1; i--)
                {
                    this.Partition[i + 1] = this.Partition[i];
                }
                this.Partition[part1 + 1] = new Partition(size2);
                this.Partition[part1 + 1].changeStatus();
                OnPartitionAdded(new Partition(size2));
            }
            else
            {
                throw new Exception("Taille de la partition voulue plus importante que la partition de départ");
            }
        }

        public void RemovePartition(Partition Partition) { 
            if (this.Partition.Remove(Partition)) { 
                OnPartitionRemoved(Partition); 
            } 
        }

        private void OnPartitionAdded(Partition Partition) { 
            PartitionAddedEventHandler handler = PartitionAdded; 
            if (handler != null) { 
                handler(this, new PartitionCollectionEventArgs(Partition)); 
            }
        }

        private void OnPartitionRemoved(Partition Partition) {
            PartitionRemovedEventHandler handler = PartitionRemoved; 
            if (handler != null) { 
                handler(this, new PartitionCollectionEventArgs(Partition)); 
            } 
        }
    }
}
