﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1_IHM.Componants
{
    public class PropertyChangedEventArgs : EventArgs {
        public PropertyChangedEventArgs(string name, object oldValue, object newValue) {
            Name = name; OldValue = oldValue; NewValue = newValue;
        }
        public virtual string Name { get; private set; }
        public virtual object OldValue { get; private set; }
        public virtual object NewValue { get; private set; } 
    }

    public delegate void PropertyChangedEventHandler(object sender, PropertyChangedEventArgs e);
    public class Partition
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public enum State {FREE, FULL}
        public Color Color { get; set; }

        public int Size { get; set; }
        public State Status { get; set; }

        public Rectangle Rect { get; set; }

        public Partition(int size)
        {
            this.Size = size;
            this.Status = State.FREE;
            this.Color = Color.Gray;
            this.Rect = new Rectangle(0, 0, 50, this.Size*100/Disc.SIZE_DISC) ;
        }

        protected void OnProperyChanged(string name, object oldValue, object newValue) { 
            PropertyChangedEventHandler handler = PropertyChanged; 
            if (handler != null) {
                handler(this, new PropertyChangedEventArgs(name, oldValue, newValue));
            } 
        }

        public void changeStatus()
        {
            if (this.Status == State.FREE)
            {
                this.Status = State.FULL;
                this.Color = Color.White;
            }
            else
            {
                this.Status = State.FREE;
                this.Color = Color.Gray;
            }
        }

        public void Offset(int dx, int dy) {
            this.Rect = new Rectangle(this.Rect.X + dx, this.Rect.Y + dy, this.Rect.Width, this.Rect.Height);
        }
    }
}
